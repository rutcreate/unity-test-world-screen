﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TestWorldScreen : MonoBehaviour {

	public Transform object1;
	public Transform object2;
	public int index = 0;

	public Image panel;
	public CanvasScaler canvasScaler;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// if (index % 2 == 0) {
		// 	panel.rectTransform.anchoredPosition = Camera.main.WorldToScreenPoint(object1.position) * 2;
		// }
		// else {
		// 	panel.rectTransform.anchoredPosition = Camera.main.WorldToScreenPoint(object2.position) * 2;
		// }
		int mod = 6;
		float padding = 5;
		float left = panel.rectTransform.sizeDelta.x * 0.5f + padding;
		float right = Camera.main.aspect * canvasScaler.referenceResolution.y - left;
		float top = Camera.main.pixelHeight - left;
		float bottom = left;

		if (index % mod == 0) {
			panel.rectTransform.anchoredPosition = new Vector2(left, bottom);
		}
		else if (index % mod == 1) {
			panel.rectTransform.anchoredPosition = new Vector2(right, bottom);
		}
		else if (index % mod == 2) {
			panel.rectTransform.anchoredPosition = new Vector2(right, top);
		}
		else if (index % mod == 3) {
			panel.rectTransform.anchoredPosition = new Vector2(left, top);
		}
		else {
			Vector3 screenPos;
			if (index % mod == 4) {
				screenPos = Camera.main.WorldToScreenPoint(object1.position);
			}
			else {
				screenPos = Camera.main.WorldToScreenPoint(object2.position);
			}
			float factorX = Camera.main.aspect * canvasScaler.referenceResolution.y / Camera.main.pixelWidth;
			float factorY = canvasScaler.referenceResolution.y / Camera.main.pixelHeight;
			float x = screenPos.x * factorX;
			float y = screenPos.y * factorY;
			panel.rectTransform.anchoredPosition = new Vector2(x, y);
		}
	}

	private void OnGUI() {
		GUI.Label(new Rect(0, 10, 500, 30), "Pixel Width = " + Camera.main.pixelWidth);
		GUI.Label(new Rect(0, 30, 500, 30), "Pixel Height = " + Camera.main.pixelHeight);
		GUI.Label(new Rect(0, 50, 500, 30), "Object 1 = " + Camera.main.WorldToScreenPoint(object1.position));
		GUI.Label(new Rect(0, 70, 500, 30), "Object 2 = " + Camera.main.WorldToScreenPoint(object2.position));
		if (GUI.Button(new Rect(0, 90, 70, 30), "Switch")) {
			index++;
		}
		GUI.Label(new Rect(0, 120, 500, 30), "Aspect Ratio = " + Camera.main.aspect);
		GUI.Label(new Rect(0, 140, 500, 30), "Aspect Ratio (pixelWidth / pixelHeight) = " + ((float)Camera.main.pixelWidth / Camera.main.pixelHeight));
		GUI.Label(new Rect(0, 160, 500, 30), "Real pixel displayed 1 = " + (Camera.main.aspect * canvasScaler.referenceResolution.y));
		GUI.Label(new Rect(0, 180, 500, 30), "Real pixel displayed 2 = " + (((float) Camera.main.pixelWidth / Camera.main.pixelHeight) * canvasScaler.referenceResolution.y));
		GUI.Label(new Rect(0, 200, 500, 30), "Anchored Position = " + panel.rectTransform.anchoredPosition);
	}
}
